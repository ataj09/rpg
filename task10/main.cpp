#include <iostream>
#include "hero.h"
#include "monster.h"


using namespace std;

hero* create_new_character( ) {
    string tempname;
    double tempstrength;
    double tempdexterity;
    double tempendurance;
    double tempintelligence;
    double tempcharisma;

    cout<<"Set name of hero: ";
    cin>>tempname;
    cout<<"Set strength of hero: ";
    cin>>tempstrength;
    cout<<"Set dexterity of hero: ";
    cin>>tempdexterity;
    cout<<"Set endurance of hero: ";
    cin>>tempendurance;
    cout<<"Set intelligence of hero: ";
    cin>>tempintelligence;
    cout<<"Set charisma of hero: ";
    cin>>tempcharisma;

    return new hero(tempname, tempstrength, tempdexterity, tempendurance, tempintelligence, tempcharisma);

}

hero* load_character() {

    string name;
    cout<<"Please enter name of hero you want to load"<<endl;
    cin>>name;

    return new hero(name);
}

void saveMonsters(monster* monsterki) {
    fstream file;
    file.open("monsters.txt", ios::out);

    if(file.good()) {
        for(int i =0; i<5; i++ ){
            file<<"Monster "<<i+1<<endl;
            file<<monsterki[i].getS()<<endl;
            file<<monsterki[i].getD()<<endl;
            file<<monsterki[i].getE()<<endl;
            file<<monsterki[i].getI()<<endl;
            file<<monsterki[i].getC()<<endl;

        }
    }

}
int main() {
    srand((unsigned int)time(NULL));
    bool isCharacter = false;
    bool isMonsters = false;

    hero* typek = nullptr;
    auto* monsterki = new monster[5];

    while(true) {
        cout<<"\n\n\n\n\n";


        cout<<"What would you like to do ?"<<endl;
        cout<<"1: Create new hero"<<endl;
        cout<<"2: Load existing hero"<<endl;
        cout<<"3: Save hero"<<endl;
        cout<<"4: Display hero stats"<<endl;
        cout<<"5: Select class"<<endl;
        cout<<"6: Generate monsters"<<endl;
        cout<<"7: Save monsters"<<endl;
        cout<<"8: exit"<<endl;

        int choice;
        cin>>choice;

        switch (choice) {

            case 1:
                typek = create_new_character();
                isCharacter = true;

                break;
            case 2:
                typek = load_character();
                isCharacter = true;

                break;
            case 3:
                if (isCharacter) typek->save();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 4:
                if (isCharacter) typek->printStats();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 5:
                if (isCharacter) typek->setClass();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 6:

                for(int i=0;i<5;i++) {
                    monsterki[i] = monster();
                    cout<<"Monster "<<i+1<<" with stats: "<<endl;
                    monsterki[i].printStats();
                    cout<<"\n";
                }
                isMonsters = true;
                break;
            case 7:
                if (isMonsters)
                saveMonsters(monsterki);
                else cout<<"You havent created any monsters yet!"<<endl;
                break;
            case 8:
                exit(0);

            default:
                cout<<"Please enter valid choice"<<endl;

        }
        cout<<"\nOperation Completed"<<endl;



    }
    return 0;
}


