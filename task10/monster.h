//
// Created by micha on 22.04.2023.
//

#ifndef RPG_MONSTER_H
#define RPG_MONSTER_H
#include "character.h"
#include <random>

using namespace std;
class monster: character {
public:
    monster();
    void printStats();
    double getS();
    double getI();
    double getD();
    double getE();
    double getC();
};


#endif //RPG_MONSTER_H
