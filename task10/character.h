//
// Created by micha on 08.05.2023.
//

#ifndef RPG_CHARACTER_H
#define RPG_CHARACTER_H
#include <string>
#include <iostream>
using namespace std;
class character {

public:

    virtual void printStats() = 0;
    virtual double getS() = 0;
    virtual double getI() = 0;
    virtual double getD() = 0;
    virtual double getE() = 0;
    virtual double getC() = 0;
protected:
    string name;
    double strength;
    double dexterity;
    double endurance;
    double intelligence;
    double charisma;
};


#endif //RPG_CHARACTER_H
