//
// Created by micha on 22.04.2023.
//

#ifndef RPG_CLASSES_H
#define RPG_CLASSES_H
#include "hero.h"


class mage {
public:
    friend class character;

    static void increaseAttribute(double*);

};

class warrior {
public:
    friend class character;

    static void increaseAttribute(double*);

};

class berserker {
public:
    friend class character;

    static void increaseAttribute(double*);
};

class thief {
public:
    friend class character;

    static void increaseAttribute(double*);

};



#endif //RPG_CLASSES_H
