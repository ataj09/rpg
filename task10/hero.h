//
// Created by micha on 22.04.2023.
//

#ifndef RPG_HERO_H
#define RPG_HERO_H
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include "classes.h"
#include "character.h"

using namespace std;
class hero : character{
public:
    hero();
    hero(string, double, double, double, double, double);
    hero(const string&);
    ~hero();
    void save();
    void printStats();
    void setClass();
    bool isConstructed = false;

    double getS();
    double getI();
    double getC();
    double getE();
    double getD();

protected:
    string name;
    double strength;
    double dexterity;
    double endurance;
    double intelligence;
    double charisma;



};


#endif //RPG_HERO_H
