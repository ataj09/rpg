//
// Created by micha on 15.05.2023.
//

#include "battleHistory.h"
battleHistory::battleHistory() {
    head = nullptr;
    tail = nullptr;
    N = 0;
}
battleHistory::~battleHistory() {
    battle_node* node;
    while(head) {
        node = head->next;
        delete head;
        head = node;
    }
}

battle_node *battleHistory::addBattle(string result) {
    auto* newBattle = new battle_node;
    newBattle->battleResult = result;
    newBattle->next = head;
    head = newBattle;
    if(!tail) tail = head;
    N++;

    if(N >= 11) {
        deleteBattle();
    }
    return head;
}

battle_node *battleHistory::deleteBattle() {
    battle_node* node;

    if(tail) {
        node = tail;
        if(node == head) head = tail = nullptr;
        else {
            tail = head;
            while(tail-> next != node) tail = tail->next;
            tail->next = nullptr;
        }
        N--;
        return node;
    }
    else return nullptr;
}

void battleHistory::displayHistory() {
    battle_node* node;
    int i = 1;
    if(!head) cout<<"No battles yet to display!"<<endl;
    else {
        node = head;
        while (node) {
            cout<<i<<": "<<node->battleResult<<endl;
            node = node -> next;
            i++;
        }
    }


}
