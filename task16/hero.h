//
// Created by micha on 22.04.2023.
//

#ifndef RPG_HERO_H
#define RPG_HERO_H
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include "classes.h"
#include "character.h"

using namespace std;
class hero : public character{
public:
    hero();
    hero(string, double, double, double, double, double);
    hero(const string&);
    ~hero();
    void save();
    void printStats();
    void setClass();
    bool isConstructed = false;
    void changeExp(int);

    int getS();
    int getI();
    int getC();
    int getE();
    int getD();

    void setS(int);
    void setI(int);
    void setC(int);
    void setE(int);
    void setD(int);

protected:
    string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
    int level;


};


#endif //RPG_HERO_H
