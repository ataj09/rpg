#include <iostream>
#include "character.h"
#include "monster.h"


using namespace std;

character* create_new_character( ) {
    string tempname;
    double tempstrength;
    double tempdexterity;
    double tempendurance;
    double tempintelligence;
    double tempcharisma;

    cout<<"Set name of character: ";
    cin>>tempname;
    cout<<"Set strength of character: ";
    cin>>tempstrength;
    cout<<"Set dexterity of character: ";
    cin>>tempdexterity;
    cout<<"Set endurance of character: ";
    cin>>tempendurance;
    cout<<"Set intelligence of character: ";
    cin>>tempintelligence;
    cout<<"Set charisma of character: ";
    cin>>tempcharisma;

    return new character(tempname,tempstrength, tempdexterity, tempendurance, tempintelligence, tempcharisma);

}

character* load_character() {

    string name;
    cout<<"Please enter name of character you want to load"<<endl;
    cin>>name;

    return new character(name);
}

void saveMonsters(monster* monsterki) {
    fstream file;
    file.open("monsters.txt", ios::out);

    if(file.good()) {
        for(int i =0; i<5; i++ ){
            file<<"Monster "<<i+1<<endl;
            file<<monsterki[i].getS()<<endl;
            file<<monsterki[i].getD()<<endl;
            file<<monsterki[i].getE()<<endl;
            file<<monsterki[i].getI()<<endl;
            file<<monsterki[i].getC()<<endl;

        }
    }

}
int main() {
    srand((unsigned int)time(NULL));
    bool isCharacter = false;
    bool isMonsters = false;

    character* typek = nullptr;
    auto* monsterki = new monster[5];

    while(true) {
        cout<<"\n\n\n\n\n";


        cout<<"What would you like to do ?"<<endl;
        cout<<"1: Create new character"<<endl;
        cout<<"2: Load existing character"<<endl;
        cout<<"3: Save character"<<endl;
        cout<<"4: Display character stats"<<endl;
        cout<<"5: Select class"<<endl;
        cout<<"6: Generate monsters"<<endl;
        cout<<"7: Save monsters"<<endl;
        cout<<"8: exit"<<endl;

        int choice;
        cin>>choice;

        switch (choice) {

            case 1:
                typek = create_new_character();
                isCharacter = true;

                break;
            case 2:
                typek = load_character();
                isCharacter = true;

                break;
            case 3:
                if (isCharacter) typek->save();
                else cout<<"You havent created/loaded any character yet!";
                break;
            case 4:
                if (isCharacter) typek->printStats();
                else cout<<"You havent created/loaded any character yet!";
                break;
            case 5:
                if (isCharacter) typek->setClass();
                else cout<<"You havent created/loaded any character yet!";
                break;
            case 6:

                for(int i=0;i<5;i++) {
                    monsterki[i] = monster();
                    cout<<"Monster "<<i+1<<" with stats: "<<endl;
                    monsterki[i].printStats();
                    cout<<"\n";
                }
                isMonsters = true;
                break;
            case 7:
                if (isMonsters)
                saveMonsters(monsterki);
                else cout<<"You havent created any monsters yet!"<<endl;
                break;
            case 8:
                exit(0);

            default:
                cout<<"Please enter valid choice"<<endl;

        }
        cout<<"\nOperation Completed"<<endl;



    }
    return 0;
}


