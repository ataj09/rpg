//
// Created by micha on 22.04.2023.
//

#include "monster.h"

monster::monster() {
    strength = rand()%10 + 1;
    dexterity = rand()%10 + 1;
    endurance = rand()%10 + 1;
    intelligence = rand()%10 + 1;
    charisma = rand()%10 + 1;
}

void monster::printStats() {
    cout<<"Strength = "<<strength<<endl;
    cout<<"Dexterity = "<<dexterity<<endl;
    cout<<"Endurance = "<<endurance<<endl;
    cout<<"Intelligence = "<<intelligence<<endl;
    cout<<"Charisma = "<<charisma<<endl;
}

double monster::getS() {
    return strength;
}

double monster::getI() {
    return intelligence;
}

double monster::getD() {
    return dexterity;
}

double monster::getE() {
    return endurance;
}

double monster::getC() {
    return charisma;
}
