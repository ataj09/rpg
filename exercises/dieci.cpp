1 #include <iostream>
2
3 using namespace std;
4
5 const int max_urzadz = 15;
6 const int max_czujn = 10;
7
8
9 int detektor[max_urzadz][max_czujn]; // tablica na wyniki pomiarow
10
11 bool odczytaj_zdarzenie();
12 void analizuj_zdarzenie();
13 unsigned long pobierz_slowo();
14
15 int main(){
    16 cout << "Program analizuje dane pomiarowe" << endl;
    17
    18 while (odczytaj_zdarzenie()){ //odebranie danych
        19 analizuj_zdarzenie();
        20 }
    21 cout << "Nie ma wiecej danych" << endl;
    22
    23 return 0;
    24 }
25
26 bool odczytaj_zdarzenie(){
    27 struct slowo_vxi {
        28 unsigned int dana :16;
        7
        29 unsigned int urzadzenie :8;
        30 unsigned int czujnik :6;
        31 unsigned int :2;
        32
        33 };
    34 //unia pozwalajaca na poszatkowanie slowa
    35 union {
        36 unsigned long cale;
        37 slowo_vxi vxi;
        38 };
    39
    40 cout << "Wczytywanie nastepnego zdarzenia...\n";
    41 //petla wczytujaca wiele slow do jednego zdarzenia
    42 while(1){
        43 cale=pobierz_slowo();
        44 if(!cale) return false;
        45 if (vxi.urzadzenie == 0xf8){
            46 cout<<"Koniec odczytu danych zdarzenia nr"<<vxi.dana<<endl;
            47 return true;
            48 }
        49 else {
            50 if(vxi.urzadzenie > max_urzadz || vxi.czujnik >=
                                                 max_czujn){
                51 continue;
                52 }
            53 detektor[vxi.urzadzenie][vxi.czujnik]=vxi.dana;
            54 }
        55 }
    56 }
57
58 void analizuj_zdarzenie(){
    59 cout<<"Analiza zdarzenia. Zadzialaly:"<<endl;
    60
    61 for (int u = 0; u < max_urzadz; u++) {
        62 for (int c = 0; c < max_czujn; c++) {
            63 if(detektor[u][c]){
                64 cout<<"\turzadzenie "<<u<<"czujnik "<<c<<", odczyt=
                                                            "<<detektor[u][c]<<endl;
                65 }
            66 }
        67 }
    68 }
69
8
70 unsigned long pobierz_slowo(){
    71 unsigned long slowo[]={
            72 0x4060658, 0x60201ff, 0x9058c, 0xf80000,
            73 328457, 100729404, 0xf80001,
            74 197827, 134417127, 84087033, 50927293,
            75 16848207, 17105686, 16847128,
            76 0xf80002,
            77 0};
    78 static int licznik;
    79 return slowo[licznik++];
    80 }