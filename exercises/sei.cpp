#include <iostream>
2 #include <vector>
3
4 using namespace std;
5
6 int main(){
    7 vector < int > container( 10, 0 );
    8 container.insert( container.begin(), 1 );
    9 container.insert( container.end(), 3, 6 );
    10 container.insert( container.begin() + 3, 2 );
    11
    12 for( int i = 0; i < container.size(); ++i )
        13 cout << container[ i ] << ’ ’;
    14 cout <<endl;
    15
    9
    16 container.erase( container.begin() );
    17 container.erase( container.begin() + 2, container.begin() + 4 );
    18 container.erase( container.begin() + 4, container.end() );
    19
    20 vector<int>::iterator it;
    21 for( it=container.begin(); it!=container.end(); ++it ){
        22 cout<< *it << ’ ’;
        23 }
    24 cout<<endl;
    25
    26 return 0;
    27 }