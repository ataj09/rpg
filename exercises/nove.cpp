#include <iostream>
2 #include <ctime>
3 #include <cstdlib>
4 #include<unistd.h>
5 using namespace std;
6
7
8 class Matrix {
    9 public:
    10 unsigned int row;
    11 unsigned int column;
    12 int **matrix;
    13
    14 Matrix() {}
    15
    16 Matrix(unsigned int r, unsigned int c): row(r), column(c){
        17 //srand(time(NULL));
        18 matrix = new int*[r];
        19 for(unsigned int i=0; i<r; i++) {
            20 matrix[i] = new int[c];
            21 for(unsigned int j=0; j<c; j++) {
                22 this->matrix[i][j] = (rand()%10)+1;
                23 }
            24 }
        25 }
    26
    27 Matrix& operator +(const Matrix& a){
        28
        29 if(a.row == this->row && a.column == this->column) {
            30 for(unsigned int i=0; i<a.row; i++) {
                31 for(unsigned int j=0; j<a.column; j++) {
                    32 this->matrix[i][j] += a.matrix[i][j];
                    33 }
                34 }
            35 return *this;
            36 }
        37 else {
            38 printf("Niezgodne wymiary Matrixy\n");
            39 return *this;
            40 }
        41 }
    10
    42
    43 Matrix operator -(const Matrix& a){
        44
        45 if(a.row == this->row && a.column == this->column) {
            46 for(unsigned int i=0; i<a.row; i++) {
                47 for(unsigned int j=0; j<a.column; j++) {
                    48 this->matrix[i][j] -= a.matrix[i][j];
                    49 }
                50 }
            51 return *this;
            52 }
        53 else {
            54 printf("Niezgodne wymiary macierzy\n");
            55 return *this;
            56 }
        57
        58 }
    59
    60 friend std::ostream& operator<<(std::ostream& out,const Matrix&
    m);
    61 };
62
63 Matrix operator +(const Matrix& a, const Matrix& b){
    64
    65 if(a.row == b.row && a.column == b.column) {
        66 Matrix c(a.row,a.column);
        67 for(unsigned int i=0; i<a.row; i++) {
            68 for(unsigned int j=0; j<a.column; j++) {
                69 c.matrix[i][j] = (a.matrix[i][j] + b.matrix[i][j]);
                70 }
            71 }
        72 return c;
        73 }
    74 else {
        75 printf("Niezgodne wymiary macierzy\n");
        76 return a;
        77 }
    78
    79 }
80
81 Matrix operator -(const Matrix& a, const Matrix& b){
    82
    83 if(a.row == b.row && a.column == b.column) {
        11
        84 Matrix c(a.row,a.column);
        85 for(unsigned int i=0; i<a.row; i++) {
            86 for(unsigned int j=0; j<a.column; j++) {
                87 c.matrix[i][j] = (a.matrix[i][j] - b.matrix[i][j])%3;
                88 }
            89 }
        90 return c;
        91 }
    92 else {
        93 printf("Niezgodne wymiary macierzy\n");
        94 return a;
        95 }
    96
    97 }
98
99 Matrix operator *(const Matrix& a, const int& b) {
    100 Matrix c(a.row,a.column);
    101 for(unsigned int i=0; i<a.row; i++) {
        102 for(unsigned int j=0; j<a.column; j++) {
            103 c.matrix[i][j] = a.matrix[i][j] * b;
            104 }
        105 }
    106 return c;
    107 }
108
109 Matrix operator *(const int& b, const Matrix& a) {
    110 Matrix c(a.row,a.column);
    111 for(unsigned int i=0; i<a.row; i++) {
        112 for(unsigned int j=0; j<a.column; j++) {
            113 c.matrix[i][j] = a.matrix[i][j] * b;
            114 }
        115 }
    116 return c;
    117 }
118
119 std::ostream& operator<<(std::ostream& out,const Matrix& m){
    120
    121 out <<"{";
    122 for(unsigned int i=0; i<m.row-1; i++) {
        123 out << "{";
        124 for(unsigned int j=0; j<m.column-1; j++) {
            125 out << m.matrix[i][j] << ",";
            126 }
        12
        127 out << m.matrix[i][m.column-1] << "},"<<endl;
        128 }
    129 out <<"{";
    130 for(unsigned int i=0; i<m.column-1; i++) {
        131 out << m.matrix[m.row-1][i] << ",";
        132 }
    133 out << m.matrix[m.row-1][m.column-1] << "}";
    134 out << "}";
    135
    136 return out;
    137
    138 }
139
140 int main() {
    141 srand(time(NULL));
    142 Matrix a=Matrix(2,3);
    143 Matrix b=Matrix(2,3);
    144 int c = 4;
    145
    146 cout << "Matrix A:" << endl;
    147 cout << a << endl<< endl;
    148
    149 cout << "Matrix B:" << endl;
    150 cout << b << endl<< endl;
    151
    152 cout << "Result of A + B:" << endl;
    153 cout << a+b << endl<< endl;
    154
    155 cout << "Result of A - B:" << endl;
    156 cout << a-b << endl<< endl;
    157 cout << "Result of C * A:" << endl;
    158 cout << c*a << endl<< endl;
    159 cout << "Result of B * C:" << endl;
    160 cout << b*c << endl<< endl;
    161 }