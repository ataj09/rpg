//
// Created by micha on 22.04.2023.
//

#include "classes.h"

double mage::increaseAttribute(double intelligence) {
    return intelligence + 5;
}

double warrior::increaseAttribute(double endurance) {
    return endurance + 5;
}

double berserker::increaseAttribute(double strength) {
    return strength + 5 ;
}

double thief::increaseAttribute(double dexterity) {
    return dexterity + 5;
}
