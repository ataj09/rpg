#include <iostream>
#include "hero.h"
#include "monster.h"
#include "battleHistory.h"



using namespace std;

hero* create_new_character( ) {
    string tempname;
    double tempstrength;
    double tempdexterity;
    double tempendurance;
    double tempintelligence;
    double tempcharisma;

    cout<<"Set name of hero: ";
    cin>>tempname;
    cout<<"Set strength of hero: ";
    cin>>tempstrength;
    cout<<"Set dexterity of hero: ";
    cin>>tempdexterity;
    cout<<"Set endurance of hero: ";
    cin>>tempendurance;
    cout<<"Set intelligence of hero: ";
    cin>>tempintelligence;
    cout<<"Set charisma of hero: ";
    cin>>tempcharisma;

    return new hero(tempname, tempstrength, tempdexterity, tempendurance, tempintelligence, tempcharisma);

}

hero* load_character() {

    string name;
    cout<<"Please enter name of hero you want to load"<<endl;
    cin>>name;

    return new hero(name);
}

void saveMonsters(monster* monsterki) {
    fstream file;
    file.open("monsters.txt", ios::out);

    if(file.good()) {
        for(int i =0; i<5; i++ ){
            file<<"Monster "<<i+1<<endl;
            file<<monsterki[i].getS()<<endl;
            file<<monsterki[i].getD()<<endl;
            file<<monsterki[i].getE()<<endl;
            file<<monsterki[i].getI()<<endl;
            file<<monsterki[i].getC()<<endl;

        }
    }

}

template <class T> class Battle {
public:
    Battle() {}
    int fight_phys(T hero, monster* opponent) {

        int hero_hp = hero->getE()*100;
        int monster_hp = opponent->getE()*100;

        int hero_dice;
        int monster_dice;

        while(hero_hp > 0 && monster_hp > 0) {
            hero_dice = rand()%12 + 1;
            monster_dice = rand()%12 + 1;

            cout<<"Hero diced: "<<hero_dice<<endl;
            cout<<"Monster diced: "<<monster_dice<<endl;

            cout<<"Hero total attack points: "<< hero->getS() + hero_dice<<endl;
            cout<<"Monster total attack points: "<< opponent->getS() + monster_dice<<endl;

            if(hero->getS() + hero_dice > opponent->getS() + monster_dice) {
                monster_hp -= 10;
                cout<<"This round was won by Hero"<<endl;
            }
            else if (hero->getS() + hero_dice < opponent->getS() + monster_dice)
            {
                hero_hp -= 10;
                cout<<"This round was won by Monster"<<endl;
            }
            else {
                cout<<"This round ended up in draw"<<endl;
            }
            cout<<"\n";
        }
        if (hero_hp > 0) return 1;
        return 0;


    }

};

int main() {
    srand((unsigned int)time(NULL));
    bool isCharacter = false;
    bool isMonsters = false;

    hero* typek = nullptr;
    monster* monsterki = new monster[5];

    Battle<character*> fight;

    battleHistory history;

    while(true) {
        cout<<"\n\n\n\n\n";


        cout<<"What would you like to do ?"<<endl;
        cout<<"1: Create new hero"<<endl;
        cout<<"2: Load existing hero"<<endl;
        cout<<"3: Save hero"<<endl;
        cout<<"4: Display hero stats"<<endl;
        cout<<"5: Select class"<<endl;
        cout<<"6: Generate monsters"<<endl;
        cout<<"7: Save monsters"<<endl;
        cout<<"8: fight with monster"<<endl;
        cout<<"9: show battle history"<<endl;
        cout<<"10: exit"<<endl;

        int choice;
        cin>>choice;

        switch (choice) {

            case 1:
                typek = create_new_character();
                isCharacter = true;

                break;
            case 2:
                typek = load_character();
                isCharacter = true;

                break;
            case 3:
                if (isCharacter) typek->save();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 4:
                if (isCharacter) typek->printStats();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 5:
                if (isCharacter) typek->setClass();
                else cout<<"You havent created/loaded any hero yet!";
                break;
            case 6:

                for(int i=0;i<5;i++) {
                    monsterki[i] = monster();
                    cout<<"Monster "<<i+1<<" with stats: "<<endl;
                    monsterki[i].printStats();
                    cout<<"\n";
                }
                isMonsters = true;
                break;
            case 7:
                if (isMonsters)
                saveMonsters(monsterki);
                else cout<<"You havent created any monsters yet!"<<endl;
                break;

            case 8:
                if(!isMonsters) {
                    cout<<"You havent created any monsters yet!"<<endl;
                    break;
                }
                if(!isCharacter) {
                    cout<<"You havent created/loaded any hero yet!";
                    break;
                }
                cout<<"Choose monster to fight from 1-5"<<endl;
                int choosen_monster;
                cin>>choosen_monster;

                if (choosen_monster < 1 || choosen_monster > 5) {
                    cout<<"Wrong monster choosen! "<<endl;
                    break;
                }
                else {
                    if(fight.fight_phys(typek, &monsterki[choosen_monster - 1]) == 1) {
                        cout<<"Hero won!"<<endl;
                        cout<<"Monster experience: "<<monsterki[choosen_monster-1].getExp()<<" was added to hero"<<endl;
                        typek->changeExp(monsterki[choosen_monster-1].getExp());
                        history.addBattle("Hero won and got " + to_string(monsterki[choosen_monster-1].getExp()) + " experience");
                    }
                    else {
                        cout<<"Monster won!"<<endl;
                        cout<<"Monster experience: "<<monsterki[choosen_monster-1].getExp()<<" was subtracted from hero"<<endl;
                        typek->changeExp(-monsterki[choosen_monster-1].getExp());
                        history.addBattle("Hero lost and got -" + to_string(monsterki[choosen_monster-1].getExp()) + " experience");
                    }
                }

                break;
            case 9:
                history.displayHistory();
                break;

            case 10:
                delete[] monsterki;
                delete typek;
                exit(0);

            default:
                cout<<"Please enter valid choice"<<endl;

        }
        cout<<"\nOperation Completed"<<endl;



    }
    return 0;
}


