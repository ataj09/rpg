//
// Created by micha on 22.04.2023.
//

#ifndef RPG_MONSTER_H
#define RPG_MONSTER_H
#include "character.h"
#include <random>

using namespace std;
class monster: public character {
public:
    monster();
    void printStats();
    int getS();
    int getI();
    int getD();
    int getE();
    int getC();
    int getExp();
};


#endif //RPG_MONSTER_H
