//
// Created by micha on 22.04.2023.
//

#include "classes.h"

void mage::increaseAttribute(int *intelligence) {
    *intelligence += 5;
}

void warrior::increaseAttribute(int *endurance) {
    *endurance += 5;
}

void berserker::increaseAttribute(int *strength) {
    *strength += 5;
}

void thief::increaseAttribute(int *dexterity) {
    *dexterity += 5;
}
