//
// Created by micha on 15.05.2023.
//

#ifndef RPG_BATTLEHISTORY_H
#define RPG_BATTLEHISTORY_H
#include <string>
#include <iostream>
using namespace std;

struct battle_node {
    string battleResult;
    battle_node *next;
};

class battleHistory {
    battle_node* head;
    battle_node* tail;
    int N;

public:
    battleHistory();
    ~battleHistory();
    battle_node* addBattle(string);
    battle_node* deleteBattle();
    void displayHistory();


};

#endif //RPG_BATTLEHISTORY_H
