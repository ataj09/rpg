//
// Created by micha on 22.04.2023.
//

#include "monster.h"

monster::monster() {
    strength = rand()%10 + 1;
    dexterity = rand()%10 + 1;
    endurance = rand()%10 + 1;
    intelligence = rand()%10 + 1;
    charisma = rand()%10 + 1;
    experience = rand()%100 + 1;
}

void monster::printStats() {
    cout<<"Strength = "<<strength<<endl;
    cout<<"Dexterity = "<<dexterity<<endl;
    cout<<"Endurance = "<<endurance<<endl;
    cout<<"Intelligence = "<<intelligence<<endl;
    cout<<"Charisma = "<<charisma<<endl;
    cout<<"Experience = "<<experience<<endl;
}

int monster::getS() {
    return strength;
}

int monster::getI() {
    return intelligence;
}

int monster::getD() {
    return dexterity;
}

int monster::getE() {
    return endurance;
}

int monster::getC() {
    return charisma;
}
int monster::getExp() {
    return experience;
}