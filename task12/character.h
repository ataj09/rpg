//
// Created by micha on 08.05.2023.
//

#ifndef RPG_CHARACTER_H
#define RPG_CHARACTER_H
#include <string>
#include <iostream>
using namespace std;
class character {

public:

    virtual void printStats() = 0;
    virtual int getS() = 0;
    virtual int getI() = 0;
    virtual int getD() = 0;
    virtual int getE() = 0;
    virtual int getC() = 0;
protected:
    string name;
    int strength;
    int dexterity;
    int endurance;
    int intelligence;
    int charisma;
    int experience;
};


#endif //RPG_CHARACTER_H
