//
// Created by micha on 22.04.2023.
//

#ifndef RPG_CHARACTER_H
#define RPG_CHARACTER_H
#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;
class character {
public:
    character(string,double,double,double,double, double);
    character(const string&);
    ~character();
    void save();
    void printStats();

private:

    string name;
    double strength;
    double dexterity;
    double endurance;
    double intelligence;
    double charisma;


};


#endif //RPG_CHARACTER_H
