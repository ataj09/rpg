#include <iostream>
#include "character.h"

using namespace std;

character create_new_character( ) {
    string tempname;
    double tempstrength;
    double tempdexterity;
    double tempendurance;
    double tempintelligence;
    double tempcharisma;

    cout<<"Set name of character: ";
    cin>>tempname;
    cout<<"Set strength of character: ";
    cin>>tempstrength;
    cout<<"Set dexterity of character: ";
    cin>>tempdexterity;
    cout<<"Set endurance of character: ";
    cin>>tempendurance;
    cout<<"Set intelligence of character: ";
    cin>>tempintelligence;
    cout<<"Set charisma of character: ";
    cin>>tempcharisma;

    return {tempname,tempstrength, tempdexterity, tempendurance, tempintelligence, tempcharisma};

}

character load_character(const string& name) {
    return {name};
}


int main() {

    character zlodziej = create_new_character();
    character janusz = load_character("janusz");
    zlodziej.printStats();
    zlodziej.save();
    janusz.save();
    janusz.printStats();
    return 0;
}
